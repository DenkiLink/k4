import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction result = new Lfraction(2, 4).pow(3);
      Lfraction answer = new Lfraction(1, 8);
      System.out.println(result.equals(answer));
   }

   private final long x;
   private final long y;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("Denominator of a fraction cannot equal to zero!");
      }
      long div = Math.abs(gcd(a, b));
      long nx = a / div;
      long ny = b / div;

      if (ny < 0) {
         nx *= -1;
         ny *= -1;
      }

      x = nx;
      y = ny;

   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return x;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return y;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return x + "/" + y;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m instanceof Lfraction) {
         return ((Lfraction) m).compareTo(this) == 0;
      } else {
         return false;
      }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(x, y);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long nx = x * m.y;
      long mx = m.x * y;
      return new Lfraction(nx + mx, y * m.y);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(x * m.x, y * m.y);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (x == 0) {
         throw new RuntimeException("Denominator of a fraction cannot equal to zero!");
      }



      return new Lfraction(y, x);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-x, y);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.x == 0) {
         throw new RuntimeException("Denominator of a fraction cannot equal to zero!");
      }
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long nx = x * m.y;
      long mx = m.x * y;
      return Long.compare(nx, mx);
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(x, y);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return x / y;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long nx = x % y;
      return new Lfraction(nx, y);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) x / y;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long n = Math.round(f * d);
      return new Lfraction(n, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (s.contains("/")) {
         try {
            String[] nums = s.split("/");
            long nx = Long.parseLong(nums[0]);
            long ny = Long.parseLong(nums[1]);
            return  new Lfraction(nx, ny);
         } catch (NumberFormatException e) {
            throw new RuntimeException("String input must be numeric! " + s);
         }
      }
      throw new RuntimeException("Incorrect string format! " + s);
   }

   public Lfraction pow(long n) {
      if (n == 0) {
         return new Lfraction(1, 1);
      } else if (n == 1) {
         return new Lfraction(x, y);
      } else if (n == -1) {
         return inverse();
      } else if (n > 1) {
         return times(pow(n - 1));
      } else {
         return pow(-n).inverse();
      }
   }

   private static long gcd(long n1, long n2) {
      /** Find greatest common divisor of two numbers
       * Code idea from https://www.baeldung.com/java-greatest-common-divisor
       */
      if (n2 == 0) {
         return n1;
      }
      return gcd(n2, n1 % n2);
   }
}

